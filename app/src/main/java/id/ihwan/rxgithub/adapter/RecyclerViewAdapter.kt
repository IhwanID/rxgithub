package id.ihwan.rxgithub.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.ihwan.rxgithub.R
import id.ihwan.rxgithub.model.Github
import kotlinx.android.synthetic.main.item_starred.view.*

class RecyclerViewAdapter : RecyclerView.Adapter<RecyclerViewAdapter.GithubViewHolder>() {

    val data = ArrayList<Github>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GithubViewHolder {
        return GithubViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_starred, parent, false))
    }

    override fun getItemCount(): Int = data.size


    override fun onBindViewHolder(holder: GithubViewHolder, position: Int) {
        holder.fullname.text = data[position].full_name
        holder.star.text = data[position].star
        holder.desc.text = data[position].description
        holder.lang.text = data[position].language
    }

    //    TODO : SetData

    class GithubViewHolder(view: View) : RecyclerView.ViewHolder(view){

        val fullname = view.fullName
        val desc = view.description
        val lang = view.language
        val star = view.star
    }
}