package id.ihwan.rxgithub.model

import com.google.gson.annotations.SerializedName

data class Github(
    val full_name: String?,
    val description: String?,
    val language: String?,
    @SerializedName("startgazers_count") val star: String?
)